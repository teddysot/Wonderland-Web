import Head from "next/head";

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2 bg-black">
      <Head>
        <title>Wonderland Roleplay Community</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center space-y-20">
        <div className="text-pink-600 font-mono font-bold text-6xl md:text-9xl animate-pulse">
          Wonderland
        </div>
        <div className="text-pink-400 font-mono font-bold text-xl md:text-3xl animate-pulse">
          Coming Soon...
        </div>
        <div className="flex flex-col items-center justify-center space-y-4 animate-pulse">
          <div className="text-pink-400 font-mono font-bold text-sm md:text-lg">
            Join us
          </div>
          <a
            href="https://discord.gg/yMd34tEASd"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              className="rounded-full h-16"
              src="https://play-lh.googleusercontent.com/0oO5sAneb9lJP6l8c6DH4aj6f85qNpplQVHmPmbbBxAukDnlO7DarDW0b-kEIHa8SQ"
              alt="discord-logo"
            />
          </a>
        </div>
      </main>
    </div>
  );
}
